/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var p, suj, pred;
function requisicao(pais, sujeito, predicado, teste){   
    p = pais; suj = sujeito; pred = predicado;
    var url = "http://dbpedia.org/sparql?default-graph-uri=http://dbpedia.org&query="; 
    var q = encodeURIComponent("&&");
    var query;
    
    if(teste===0){

        query = "PREFIX dbpedia-owl: <http://dbpedia.org/ontology/> select distinct ?objeto where {?x a " + sujeito + ". ?x rdfs:label ?lblpais. ?x " + predicado + " ?y. ?y rdfs:label ?objeto. Filter (regex(?lblpais, '^" + pais + "$', 'i')"+ q +"langMatches(lang(?objeto),\"PT\"))}";   
    
    }
    if(teste===1){
    
        query = "PREFIX dbpedia-owl: <http://dbpedia.org/ontology/> select distinct ?objeto where {?x a " + sujeito + ". ?x rdfs:label ?lblpais. ?x " + predicado + " ?objeto. Filter (regex(?lblpais, '^" + pais + "$', 'i')"+ q +"langMatches(lang(?objeto),\"PT\"))}";   
    
    }
    
    var queryUrl = url+query+"&format=json";

    xhr = new XMLHttpRequest();
    xhr.open('GET', queryUrl, true);                    
    xhr.send();
    xhr.onreadystatechange = processRequest;

}

function processRequest(e){
    if (xhr.readyState === 4 && xhr.status === 200) {
        
        var response = JSON.parse(xhr.responseText);
        
        if(response.results.bindings[0]===undefined);{
            requisicao(p,suj,pred,1);
        }
        
        var valor = response.results.bindings[0].objeto.value;

        var pais = document.getElementById('campo_texto').value;
        
        document.getElementById('campo_texto').value = pais + " " + valor;

    }  
    document.getElementById('campo_texto').disabled = false;
}
