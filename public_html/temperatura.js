/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function requisicao_temp(cidade){   

    var url = "http://api.openweathermap.org/data/2.5/weather?q=" + cidade + "&APPID=643cc4f12b17146697a7d3fdb67b057a"; 

    xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);                    
    xhr.send();
    xhr.onreadystatechange = processRequestTemp;

}

function processRequestTemp(e){
    if (xhr.readyState === 4 && xhr.status === 200) {
        var response = JSON.parse(xhr.responseText);

        var tempkelvin = response.main.temp;

        var tempcelsius = tempkelvin - 273.16;
        var temp = tempcelsius.toFixed(2);
        var texto = document.getElementById('campo_texto').value;
        document.getElementById('campo_texto').value = texto + " " + temp + " ºC";
        document.getElementById('campo_texto').disabled = false;


    }                    
}

