/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var res;

function loadJSON() {   

    var xobj = new XMLHttpRequest();
        xobj.overrideMimeType("application/json");
    xobj.open('GET', 'padroes.json', true);
    xobj.onreadystatechange = function () {
          if (xobj.readyState === 4 && xobj.status === 200) {
            res = JSON.parse(xobj.responseText);
          }
    };
    xobj.send(null);  
}
 
function processamento_exp(texto){
    
    var sujeito;
    var predicado;
     
    var partes_texto = texto.split(/\s/);
    
    var prop = partes_texto[1];
    for(i=2;i<partes_texto.length;i++){
        if(partes_texto[i] === "da" || partes_texto[i] === "de" || partes_texto[i] === "do" || partes_texto[i] === "dos"){
            var inicio_nome = i+1;
            break;
        }
        prop = prop + " " + partes_texto[i];

    }
    var nome_var = partes_texto[inicio_nome];

    for(i=inicio_nome+1;i<partes_texto.length;i++){
        if(partes_texto[i] === "é"){
            break;
        }
        nome_var = nome_var + " " + partes_texto[i];

    }
    
    if(prop === "valor"){
//         requisicao_rss(nome_var);
         requisicao_moeda(nome_var);
         
    }else if (prop === "temperatura"){
        
         requisicao_temp(nome_var);
         
    }else{
        loadJSON();
        var len = res.triplas.length;
        
        for(var i=0; i < len; i++){
            if (res.triplas[i].palavra === prop){
                sujeito = res.triplas[i].opcoes[0].sujeito;
                predicado = res.triplas[i].opcoes[0].predicado;
                break;
            }
        }
        requisicao(nome_var,sujeito,predicado,0);
        
    }
    
}
